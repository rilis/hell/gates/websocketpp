## Websocketpp proxy repository for hell

This is proxy repository for [websocketpp librarary](https://github.com/zaphoyd/websocketpp), which allow you to build and install it using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of websocketpp using hell, have improvement idea, or want to request support for other versions of websocketpp, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in websocketpp itself please create issue on [websocketpp issue tracker](https://github.com/zaphoyd/websocketpp/issues), because here we don't do any kind of websocketpp development.